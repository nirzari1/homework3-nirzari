﻿#include <iostream>
#include "Vector.h"
#define TWO 2
#define NOT_ACCESSABLE -1
#define ACCESSABLE 1
#define _ERROR_VALUE -9999
/*
Allocaes memory in size N for the vector, and keeps its value in capacity and resize factor		
Input: size of vector to start with
Output: none
*/
Vector::Vector(int n)
{
	if (n < TWO)
	{
		n = TWO;
	}
	_elements = new int[n]; // allocates memory
	_size = 0;
	_capacity = n;
	_resizeFactor = n; // saves number n inside vector's fields
}
/*
Destructor for vector
*/
Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr; // removes the allocated memory for the array _elements
}
/*
Gets size of vector array used spots (how many indexes are used)
*/
int Vector::size() const
{
	return _size;
}
/*
Gets capacity of vector (allocated memory)
*/
int Vector::capacity() const
{
	return _capacity;
}
/*
Gets resizefactor of vector
*/
int Vector::resizeFactor() const
{
	return _resizeFactor;
}
/*
Checks if any of vector indexes are being used, if they aren't returns true, else - false
*/
bool Vector::empty() const
{
	bool returnValue = true;
	if (_size > 0) // if any index is being used size is higher than 0
	{
		returnValue = false;
	}
	return returnValue;
}
/*
Adds an extra element in the last index of used indexes in vector array
*/
void Vector::push_back(const int& val)
{
	int* newArray = NULL;
	int i = 0;
	if (_size < _capacity) // if not all available indexes are being used in the array
	{
		_elements[_size] = val;
		_size += 1; // adds the element to last index and adds 1 to size
	}
	else // if all available indexes are being used
	{
		newArray = new int[_capacity + _resizeFactor]; //  creates a new array but bigger than _elements capacitys
		for (i = 0; i < _size; i++)
		{
			newArray[i] = _elements[i]; // copies _elements values to newarray
		}
		newArray[_size] = val; // adds the new element at end of available indexes
		_size += 1;
		delete[] _elements; // deletes the memory for _elements
		_elements = newArray;
		_capacity += _resizeFactor; // replaces elements with newarray (newarray == _elements) and increases the capacity
	}
}
/*
Pops the last available element in vector array
*/
int Vector::pop_back()
{
	int returnValue = 0;
	if (empty()) // if the vector is empty (no available elements)
	{
		returnValue = _ERROR_VALUE;
		std::cout << "error: pop from empty vector\n"; // lets user know nothing can be popped
	}
	else
	{
		returnValue = _elements[_size - 1];
		_elements[_size - 1] = NOT_ACCESSABLE;
		_size--; // pops last elements and reduces size
	}
	return returnValue; // returns popped value
}
/*
This function gets a number and makes sure the capacity is atleast that number
Input: a number
Output: none
*/
void Vector::reserve(int n)
{
	int i = 0;
	int* newArray;
	int num = 0;
	if (_capacity < n) // if the capacity is below the required number
	{
		num = n - _capacity;
		num = num / _resizeFactor;
		num++;
		num = num * _resizeFactor;
		// This calculation makes it so that num + capacity > n (everytime) - calcuation is based on resize factor
		newArray = new int[num + _capacity]; // creates new array that has atleast or higher N indexes
		for (i = 0; i < _size; i++)
		{
			newArray[i] = _elements[i]; // copies elements to new array
		}
		delete[] _elements; // deletes memory allocated to elements array
		_elements = newArray;
		_capacity += num; // _elements is now a new array with bigger capcaity and adds num  to capacity (capacity == _elements indexes allocated)
	}
}
/*
This function changes _elements array capacity to parameter N,
*/
void Vector::resize(int n)
{
	int i = 0;
	if (n <= _capacity) // if n is lower or equal to capacity
	{
		if (_size > n) // if n is lower than size
		{
			for (i = n; i < _size; i++)
			{
				_elements[i] = NOT_ACCESSABLE; // all indexes from n-_size are changed to not accessable
			}
		}
		else if (_size < n) // if n is higher than size
		{
			for (i = _size; i < n; i++)
			{
				_elements[i] = ACCESSABLE; // all numbers from _size to n changed to accessable
			}
		}
	}
	else if (n > _capacity) // if n is higher than capacity
	{
		reserve(n); // uses func reserve to make the array biger
		for (i = _size; i < n; i++)
		{ 
			_elements[i] = ACCESSABLE; // changes all available indexes from size to n to accessable
		}
	}
	_size = n; // size is equal to n at the end
}
/*
This function puts in all available indexes an entered value as parameter
*/
void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < _size; i++)
	{
		_elements[i] = val; // puts in all available indexses the param val
	}
}
/*
This function is the same as the other resize, it just puts a value inside all of the new indexes she adds
*/
void Vector::resize(int n, const int& val)
{
	int i = 0;
	if (n <= _capacity)
	{
		if (_size > n)
		{
			for (i = n; i < _size; i++)
			{
				_elements[i] = NOT_ACCESSABLE;
			}
		}
		else if (_size < n)
		{
			for (i = _size; i < n; i++)
			{
				_elements[i] = val;
			}
		}
	}
	else if (n > _capacity)
	{
		reserve(n);
		for (i = _size; i < n; i++)
		{
			_elements[i] = val;
		}
	}
	_size = n;
}
/*
This function is a copy contrsuctor
*/
Vector::Vector(const Vector& other)
{
	int i = 0;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_elements = new int[_capacity]; // copies everything and allocates memory for a new array
	for (i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i]; // copies the array to the new array (i couldn't find a fix for this warning)
	}
}
/*
This function is a copy operator
*/
Vector& Vector::operator=(const Vector& other)
{
	int i = 0;
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}
	delete[] this->_elements; // release old memory
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor; // copies everything
	if (_capacity < TWO)
	{
		_capacity = TWO;
	}
	// deep copy dynamic fields (pointers/arrays)
	this->_elements = new int[_capacity];
	for (i = 0; i < _size; i++)
	{
		// copies cell by cell
		this->_elements[i] = other._elements[i];
	}
	return *this;
}
/*
This function is used as an array operator for the vector name
*/
int& Vector::operator[](int n) const
{
	if (n > _size || n < 0) 
	{
		std::cout << "Out of index range!\n";
		if (_size > 0) // if the n (V[n]) is lower than 0 or higher than size, its out of range) and if there are available indexes it returns the first index
		{
			int& returnValue = _elements[0];
			return returnValue; // returns first index in array
		}
		_exit(1); // Nothing in array at all (if it got into the IF statement it wont get here, if it didnt means array is empty)
	}
	else
	{
		int& returnValue = _elements[n];
		return returnValue; // if N is inside index range, it returns [n] index
	}
}